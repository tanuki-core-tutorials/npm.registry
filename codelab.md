id: use-gitlab-npm-registry
summary: Use GitLab NPM Registry on GitLab.com
authors: @k33g_org

# How to use GitLab NPM Registry on GitLab.com
<!-- ------------------------ -->
## Introduction

We'll see how to use the [GitLab NPM Registry](https://docs.gitlab.com/ee/user/packages/npm_registry/index.html) on GitLab.com

> Remark: there are several ways to authenticating to the GitLab NPM Registry. In this tutorial, we'll use a **[CI job token](https://docs.gitlab.com/ee/user/packages/npm_registry/index.html#authenticating-with-a-ci-job-token)**

- Author: [@k33g](https://gitlab.com/k33g)
- Source code of this tutorial: [https://gitlab.com/tanuki-core-tutorials/npm.registry](https://gitlab.com/tanuki-core-tutorials/npm.registry)

<!-- ------------------------ -->
## Requirements

You need:

- NodeJS + Npm
- a Code editor
- git
- a GitLab account

<!-- ------------------------ -->
## Initialization
Duration: 5

First create a project on GitLab (mine is named `cyst`) and you can find it here [https://gitlab.com/tanuki-core-tutorials/cyst](https://gitlab.com/tanuki-core-tutorials/cyst).
Then, git clone your project on your computer.

Then change of directory:

```bash
cd cyst # if the name of your project is cyst
```

Initialize your npm project with this command:

```bash
npm init  
```

And answer the questions:

```bash
package name: (cyst) 
version: (1.0.0) 
description: Cyst is a helper to ease the use of the JavaScript localStorage object of the browser
entry point: (index.js) 
test command: 
git repository: (git@gitlab.com:tanuki-core-tutorials/cyst.git) 
keywords: localstorage
author: @k33g_org
license: (ISC) MIT
```

At the end you should obtain this kind of output:

```json
{
  "name": "cyst",
  "version": "1.0.0",
  "description": "Cyst is a helper to ease the use of the JavaScript localStorage object of the browser",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": "git+ssh://git@gitlab.com/tanuki-core-tutorials/cyst.git"
  },
  "keywords": [
    "localstorage"
  ],
  "author": "@k33g_org",
  "license": "MIT",
  "bugs": {
    "url": "https://gitlab.com/tanuki-core-tutorials/cyst/issues"
  },
  "homepage": "https://gitlab.com/tanuki-core-tutorials/cyst#readme"
}
```

This will create a new file in your directory named `package.json`

👏 you are ready for the next step

<!-- ------------------------ -->
## Add JavaScript code
Duration: 5

### Create an amazing library

Create in the `cyst` directory a new `index.js` file, with this content:

```javascript
export const cyst = {
  items: Object.entries(localStorage),
  getItem: (key) => localStorage.getItem(key),
  setItem: (key, value) => localStorage.setItem(key, value)
}
```

> a good practice is to add a `README.md` file, but it's optional today 😉


<!-- ------------------------ -->
## How to prepare the publication of the library to your GitLab registry
Duration: 5

### My project belongs belongs to a group

My project `cyst` belongs to the `tanuki-core-tutorials` group on `gitlab.com`, so you need to update the `package.json` file like that:

First, change the `name` ket at the top of the file. Prefix the name of your project with `@` + the name of the group + `/` (`@tanuki-core-tutorials/cyst`)

```json
{
  "name": "@tanuki-core-tutorials/cyst",
```

Then add this at the bottom of the file:

```json
  "publishConfig": {
    "@tanuki-core-tutorials:registry":"https://gitlab.com/api/v4/projects/17854780/packages/npm/"
  }
}
```

> - 👋 `17854780` is the ID of my project which can be found on the home page of the project
> - use only the name of the group `@tanuki-core-tutorials` for the registry section

#### My project doesn't belong to a group

😃 easy, replace the name of the group by your handle

### Add a `.npmrc` file

Create a `.npmrc` with this content:

```
@tanuki-core-tutorials:registry=https://gitlab.com/api/v4/packages/npm/
//gitlab.com/api/v4/packages/npm/:_authToken=${CI_JOB_TOKEN}
//gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}
```

> - use only the name of the group `@tanuki-core-tutorials` for the registry section

Create a `.npmignore` file with this content:

```
.gitlab-ci.yml
.npmrc
```

<!-- ------------------------ -->
## How to publish
Duration: 5

### `.gitlab-ci.yml`

Add a `.gitlab-ci.yml` file to your project:

```yaml
stages:
  - 🚢publish

📝publish-cyst-library:
  stage: 🚢publish
  image: node:12.0-slim
  script: |
    npm publish
```

### Push your project

```bash
git add .
git commit -m "🎉 first release"
git push
```

The CI will be triggered, and some seconds (or minutes) later you will find your new npm package in the packages list (go to the left menu, choose **Packages** and then **List**):

![alt packages](packages-list.png)

<!-- ------------------------ -->
## How to use the new package
Duration: 5

To use your package in a project, in the list of the packages, click on your latest package to get the details:

![alt package](package-details.png)

On the right, you can read the commands: 
- to install your package (**Installation** panel): `npm i @tanuki-core-tutorials/cyst`
- to setup the access to the registry (**Registry Setup** panel): `echo @tanuki-core-tutorials:registry=https://gitlab.com/api/v4/packages/npm >> .npmrc`

Somewhere on your laptop, type the following commands:

```bash
mkdir sandbox
cd sandbox
echo @tanuki-core-tutorials:registry=https://gitlab.com/api/v4/packages/npm >> .npmrc
npm i @tanuki-core-tutorials/cyst
```

🎉 the package is installed! 

```bash
.
├── node_modules
│  └── @tanuki-core-tutorials
│     └── cyst
│        ├── index.js
│        ├── package.json
│        └── README.md
└── package-lock.json
```

<!-- ------------------------ -->
## Resources

- [The Cyst project used for this tutorial](https://gitlab.com/tanuki-core-tutorials/cyst)
- [GitLab NPM Registry documentation](https://docs.gitlab.com/ee/user/packages/npm_registry/index.html)
- [Creating a simple npm library to use in and out of the browser](https://www.intricatecloud.io/2020/02/creating-a-simple-npm-library-to-use-in-and-out-of-the-browser/)






